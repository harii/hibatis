package hibatis;

/**
 * Created by huangdachao on 2018/6/13 16:24.
 */

import hibatis.annotation.*;

import java.util.List;

/**
 * Mapper接口基础类
 * @param <T> 实体类类型
 * @param <IdType> 主键类型
 */
public interface BaseMapper<T, IdType> extends EmptyMapper<T, IdType> {

    @ExecuteInsert
    int insert(T entity);

    @ExecuteInsert
    int insertArray(T[] array);

    @ExecuteInsert
    int insertAll(List<T> list);

    @ExecuteUpdate
    int update(T entity);

    @ExecuteUpdate
    int updateByPk(@IdParam IdType key, @ValueParam Object val);

    @ExecuteUpdate
    int updateByCondition(@FilterParam Object condition, @ValueParam Object val);

    @ExecuteSelect
    List<T> selectList(Object query);

    @ExecuteCount
    long selectCount(Object query);

    @ExecuteSelect(selectOne = true)
    T selectOne(Object query);

    @ExecuteSelect
    T selectById(@IdParam IdType key);

    @ExecuteSelect
    List<T> selectByIds(@IdParam List<IdType> keys);

    @ExecuteDelete
    int deleteById(@IdParam IdType key);

    @ExecuteDelete
    int deleteByIds(@IdParam List<IdType> keys);

    @ExecuteDelete
    int deleteByCondition(Object condition);
}
