package hibatis;

import hibatis.annotation.Limit;
import hibatis.annotation.Offset;
import hibatis.annotation.Sort;

/**
 * Created by dave on 18-7-1 下午4:38.
 */
public class PagedQuery {
    @Limit
    private long pageSize = 20;

    private long pageIndex = 1;

    @Sort
    private String sort;

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public long getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(long pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Offset
    public long getOffset() {
        return (pageIndex - 1) * pageSize;
    }
}
