package hibatis.ext;

import hibatis.annotation.GeneratedSql;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by huangdachao on 2018/6/25 22:27.
 */
public class InterceptorContext {
    private Class<?> mapperClass;
    private Class<?> entityClass;
    private Method method;
    private Object[] args;
    private GeneratedSql.Type type;
    private List<AdditionalFilter> additionalFilters = new ArrayList<>();
    private List<AdditionalColumnValue> additionalColumnValues = new ArrayList<>();

    public InterceptorContext(Class<?> mapperClass, Class<?> entityClass, Method method,
                              Object[] args, GeneratedSql.Type type) {
        this.mapperClass = mapperClass;
        this.entityClass = entityClass;
        this.method = method;
        this.args = args;
        this.type = type;
    }

    public Class<?> getMapperClass() {
        return mapperClass;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }

    public Method getMethod() {
        return method;
    }

    public Object[] getArgs() {
        return args;
    }

    public GeneratedSql.Type getType() {
        return type;
    }

    public List<AdditionalFilter> getAdditionalFilters() {
        return additionalFilters;
    }

    public List<AdditionalColumnValue> getAdditionalColumnValues() {
        return additionalColumnValues;
    }

    public void addAdditionalFilter(AdditionalFilter af) {
        this.additionalFilters.add(af);
    }

    public void addAdditionalColumnValue(AdditionalColumnValue acv) {
        this.additionalColumnValues.add(acv);
    }
}
