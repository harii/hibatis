package hibatis.support;

import hibatis.support.parse.EntityMeta;
import hibatis.support.parse.Table;
import hibatis.support.parse.TableColumn;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by huangdachao on 2018/6/15 16:24.
 */
public class StringUtils {
    private static final Pattern TABLE_WITH_COLUMN_PATTERN = Pattern.compile("\\$\\(([a-zA-Z_][a-zA-Z0-9_]*(\\.[a-zA-Z_][a-zA-Z0-9_]*)?)\\)");

    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    public static String snakeCase(String name) {
        StringBuilder result = new StringBuilder();
        if (name != null && name.length() > 0) {
            result.append(name.substring(0, 1).toLowerCase());
            for (int i = 1; i < name.length(); i++) {
                char s = name.charAt(i);
                if (s >= 'A' && s <= 'Z') {
                    result.append("_");
                    result.append(Character.toLowerCase(s));
                } else {
                    result.append(s);
                }
            }
        }
        return result.toString();
    }

    /**
     * 替换字符串中的表名表达式和实体类字段表达式
     * @param placeholder
     * @param em
     * @param tblAlias
     * @return
     */
    public static String normalizeTableAndFieldExpression(String placeholder, EntityMeta em, Map<Table, String> tblAlias) {
        String p1 = replaceTableWithAlias(placeholder, tblAlias);
        return replaceFieldWithTableColumn(p1, em, tblAlias);
    }

    /**
     * 使用表别名替换placeholder中的表名
     * @param placeholder
     * @param tblAlias
     * @return
     */
    public static String replaceTableWithAlias(String placeholder, Map<Table, String> tblAlias) {
        Matcher matcher = TABLE_WITH_COLUMN_PATTERN.matcher(placeholder);
        StringBuffer buffer = new StringBuffer();
        while (matcher.find()) {
            String[] arr = matcher.group(1).split("\\.");
            matcher.appendReplacement(buffer, tblAlias.get(new Table(arr[0], arr.length > 1 ? arr[1] : "")));
        }
        matcher.appendTail(buffer);
        return buffer.toString();
    }

    /**
     * 使用表别名和列名替换placeholder中的表名
     * @param placeholder
     * @param em
     * @param tblAlias
     * @return
     */
    public static String replaceFieldWithTableColumn(String placeholder, EntityMeta em, Map<Table, String> tblAlias) {
        Matcher matcher = TableColumn.ENTITY_FIELD_TABLE_COLUMN_PATTERN.matcher(placeholder);
        StringBuffer buffer = new StringBuffer();
        while (matcher.find()) {
            String field = matcher.group(1);
            TableColumn tc = em.getFieldsMap().get(field);
            if (tc == null) {
                throw new RuntimeException("没有找到字段" + em.getEntityClass().getName() + "." + field);
            }
            matcher.appendReplacement(buffer, tblAlias.get(tc.toTable()) + "." + tc.column);
        }
        matcher.appendTail(buffer);
        return buffer.toString();
    }
}
