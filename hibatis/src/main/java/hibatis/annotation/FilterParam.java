package hibatis.annotation;

import hibatis.ConflictAction;
import hibatis.FilterType;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/15 14:47.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface FilterParam {

    @AliasFor(attribute = "field")
    String value() default "";

    /**
     * 当过滤参数是简单数据类型时，可以指定过滤参数对应的实体类字段名
     * @return
     */
    @AliasFor(attribute = "value")
    String field() default "";

    /**
     * 当过滤参数是简单数据类型时，可以指定过滤参数对应的数据库表字段名
     * @return
     */
    String column() default "";

    /**
     * 当过滤参数是简单数据类型时，可以指定数据库表名，当column不为空时有效
     * @return
     */
    String table() default "";

    /**
     * 表别名，用于多态表自Join时
     * @return
     */
    String tblAlias() default "";

    /**
     * 关联表的关联关系，当column不为空时有效
     * @return
     */
    Join[] join() default {};

    /**
     * 过滤类型
     * @return
     */
    FilterType type() default FilterType.eq;

    String placeholder() default "";   // 过滤条件值的占位符，比如 "year(#{$@})"，这里的'$@'将被过滤字段名替换

    /**
     * 在过滤条件中的排序，值小的在过滤条件中靠前位置。
     */
    int order() default Filter.DEFAULT_ORDER;

    /**
     * 对于Number或int, long, float, double类型字段，是否在参数值为0时忽略本过滤条件
     * @return
     */
    boolean ignoreOnZero() default false;

    /**
     * 当相同的TableColumn过滤条件存在时的行为
     * @return
     */
    ConflictAction conflict() default ConflictAction.coexist;
}
