package hibatis.annotation;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/13 16:17.
 */
@Target(ElementType.TYPE)
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface Entity {
    /**
     * 表名，主表名
     * @return
     */
    String table();

    /**
     * 1对1关联表定义
     * @return
     */
    Join[] join() default {};
}
